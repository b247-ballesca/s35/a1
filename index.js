const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 4004;

// Allows us to read json data
app.use(express.json());

// Allows us to read data from forms
app.use(express.urlencoded({extended:true}));

	// [ SECTION ] MongoDB Connection
	// Connect to the database by passing in your connection string, remember to replace the password

	// Syntax:
		// mongoose.connect("MongoDB connection string", { useNewUrlParser : true });

// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://mnballesca0229:09283542019@zuitt-bootcamp.fakxe3v.mongodb.net/s35-activity?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.on("open", () => console.log("We're connected to the cloud database"));

// [ SECTION ] Moongoose Schemas
	// Schemas determine the structure of the documents to be written in the database

const taskSchema = new mongoose.Schema({
	name : String,
	status : {
		type : String,
		default : "pending"
	}
})

// [ SECTION ] Models
	// Uses schemas and are used to create/instantiate objects that correspond to the schema

// MongoDB Model should always be Capital & Singular
const Task = mongoose.model("Task", taskSchema);

// [ SECTION ] Routes

app.post("/tasks", (request, response) => {
	Task.findOne({name: request.body.name}, (error, result) => {
		if(result != null && result.name == request.body.name){
			return response.send('Duplicate task found!')
		} 

		let newTask = new Task({
			name: request.body.name
		})

		newTask.save((error, savedTask) => {
			if(error){
				return console.error(error)
			} else {
				return response.status(200).send('New task created!')
			}
		})
	})
})


app.get("/tasks", (req,res) => {
	Task.find({}, (err, result) => {
		if (err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				data : result
			})
		}
	})
})





// Activity

const taskSchemaSignUp = new mongoose.Schema({
	username : String,
	password : String
})


const SignUp = mongoose.model("SignUp", taskSchemaSignUp);



app.post("/signup", (request, response) => {
	SignUp.findOne({username: request.body.username}, (error, result) => {
		if(result != null && result.name == request.body.name){
			return response.send('Duplicate task found!')
		} 

		let newSignUp = new SignUp({
			username: request.body.username,
			password: request.body.password
		})

		newSignUp.save((error, savedTask) => {
			if(error){
				return console.error(error)
			} else {
				return response.status(200).send('New user registered!')
			}
		})
	})
})

// Listen to the port
app.listen(port, () => console.log(`Server is now running at port ${port}`));


